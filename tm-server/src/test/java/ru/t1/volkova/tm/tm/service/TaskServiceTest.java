package ru.t1.volkova.tm.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.service.dto.ProjectDTOService;
import ru.t1.volkova.tm.service.dto.TaskDTOService;
import ru.t1.volkova.tm.service.dto.UserDTOService;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TaskServiceTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    private static String USER_ID;

    @NotNull
    private static final List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private static IProjectDTOService projectService;

    @NotNull
    private static ITaskDTOService taskService;

    @NotNull
    private static IUserDTOService userService;

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        projectService = context.getBean(ProjectDTOService.class);
        taskService = context.getBean(TaskDTOService.class);
        userService = context.getBean(UserDTOService.class);

        @NotNull final UserDTO user = userService.create("user", "user", "user@mail.ru");
        USER_ID = user.getId();
        createTasks();
    }

    private static void createTasks() {
        @Nullable final ProjectDTO projectDTO = projectService.create(USER_ID, "Project", "Description");
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @Nullable final TaskDTO task = taskService.create(USER_ID, "Task" + i, "Description" + i);
            task.setProjectId(projectDTO.getId());
            taskList.add(task);
        }
    }

    @Test
    public void testCreate(
    ) {
        int size = taskService.getSize(USER_ID);
        @NotNull final TaskDTO task = taskService.create(USER_ID, "new_task", "new description");
        Assert.assertEquals(task, taskService.findOneById(USER_ID, task.getId()));
        Assert.assertEquals(size + 1, taskService.getSize(USER_ID));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateUserId(
    ) {
        taskService.create(null, "new_task", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) {
        taskService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) {
        taskService.create(USER_ID, "new", null);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "new taskUpd";
        @NotNull final String newDescription = "new taskUpd";
        @Nullable final TaskDTO task = taskList.get(0);
        @NotNull final String oldName = task.getName();
        @NotNull final String oldDescription = task.getDescription();
        taskService.updateById(task.getUserId(), task.getId(), newName, newDescription);
        @Nullable final TaskDTO taskUpd = taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertEquals(newName, taskUpd.getName());
        Assert.assertEquals(newDescription, taskUpd.getDescription());
        taskService.updateById(task.getUserId(), task.getId(), oldName, oldDescription);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) {
        @NotNull final String newName = "new task";
        @NotNull final String newDescription = "new task";
        @NotNull final String id = "non-existent-id";
        taskService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        taskService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) {
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) {
        taskService.updateById(taskList.get(0).getUserId(), taskList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "new nameUpd";
        @NotNull final String newDescription = "new descUpd";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        @Nullable final TaskDTO task = taskService.updateByIndex(USER_ID, index, newName, newDescription);
        Assert.assertEquals(newName, task.getName());
        Assert.assertEquals(newDescription, task.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) {
        taskService.updateByIndex(USER_ID, 10, "name", "new description");
        taskService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        taskService.updateByIndex(USER_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final String id = taskList.get(0).getId();
        @NotNull final String userId = taskList.get(0).getUserId();
        @Nullable final TaskDTO task = taskService.changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() {
        taskService.changeTaskStatusById(USER_ID, null, Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundById() {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        @Nullable final TaskDTO task = taskService.changeTaskStatusByIndex(USER_ID, index, Status.IN_PROGRESS);
        if (task == null) return;
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() {
        taskService.changeTaskStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        taskService.changeTaskStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testChangeStatusTaskNotFoundByIndex() {
        taskService.changeTaskStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        taskService.changeTaskStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(taskService.getSize(USER_ID));
        Assert.assertNotNull(taskService.findOneById(USER_ID, taskList.get(index).getId()));
    }

    @Test(expected = TaskNotFoundException.class)
    public void testFindOneByIdNegative() {
        Assert.assertNotNull(taskService.findOneById(USER_ID, "non-existent"));
    }

    @Test
    public void testFindAllByProjectId() {
        @Nullable String projectId = null;
        @Nullable String userId = null;
        for (@NotNull final TaskDTO task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            break;
        }

        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        Assert.assertNotNull(tasks);
    }

    @Test
    public void findAllByProjectIdNegative() {
        @Nullable String userId = null;
        for (@NotNull final TaskDTO task : taskList) {
            userId = task.getUserId();
            break;
        }
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, "non-existent");
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void findAllByProjectIdProjectNull() {
        taskService.findAllByProjectId(USER_ID, null);
        taskService.findAllByProjectId(USER_ID, "");
    }

}
