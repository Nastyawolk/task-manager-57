package ru.t1.volkova.tm.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.dto.IProjectDTOService;
import ru.t1.volkova.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.volkova.tm.api.service.dto.ITaskDTOService;
import ru.t1.volkova.tm.api.service.dto.IUserDTOService;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.TaskDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.service.dto.ProjectDTOService;
import ru.t1.volkova.tm.service.dto.TaskDTOService;
import ru.t1.volkova.tm.service.dto.UserDTOService;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ProjectTaskTest extends AbstractSchemeTest {

    private static final int NUMBER_OF_ENTRIES = 5;

    private static String USER_ID;

    @NotNull
    private static final List<ProjectDTO> projectList = new ArrayList<>();

    @NotNull
    private static final List<TaskDTO> taskList = new ArrayList<>();

    @NotNull
    private static IProjectDTOService projectService;

    @NotNull
    private static ITaskDTOService taskService;

    @NotNull
    private static IUserDTOService userService;

    @NotNull
    private static IProjectTaskDTOService projectTaskService;

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        projectService = context.getBean(ProjectDTOService.class);
        taskService = context.getBean(TaskDTOService.class);
        userService = context.getBean(UserDTOService.class);
        projectTaskService = context.getBean(IProjectTaskDTOService.class);

        @NotNull final UserDTO user = userService.create("user", "user", "user@mail.ru");
        USER_ID = user.getId();
        createProjectsAndTasks();
    }

    private static void createProjectsAndTasks() {
        @Nullable ProjectDTO project = projectService.create(USER_ID, "Project1", "Description");
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @Nullable TaskDTO task = taskService.create(USER_ID, "Task" + i, "Description" + i);
            projectList.add(project);
            taskList.add(task);
        }
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<ProjectDTO> userProjects = projectService.findAll(USER_ID);
        @Nullable List<TaskDTO> userTasks = taskService.findAll(USER_ID);
        final int projectIndex = random.nextInt(userProjects.size());
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        @NotNull final TaskDTO bindedTask = projectTaskService.bindTaskToProject(USER_ID, projectId, taskId);
        Assert.assertEquals(projectId, bindedTask.getProjectId());
        Assert.assertEquals(taskId, bindedTask.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskWithEmptyProjectId() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<TaskDTO> userTasks = taskService.findAll(USER_ID);
        if (userTasks == null) return;
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        projectTaskService.bindTaskToProject(USER_ID, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskWithEmptyTaskId() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<ProjectDTO> userProjects = projectService.findAll(USER_ID);
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.bindTaskToProject(USER_ID, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskWithTaskNotFound() throws Exception {
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(USER_ID, projectId, "non-existent");
    }

    @Test
    public void testRemoveByProjectId() throws SQLException {
        @Nullable String projectId = null;
        @Nullable String userId = null;
        for (@NotNull final TaskDTO task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            break;
        }
        if (projectId == null && userId == null) return;
        int projectSize = projectService.getSize(userId);
        projectTaskService.removeProjectById(userId, projectId);
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
        Assert.assertNotEquals(projectSize, projectService.getSize(userId));
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveByEmptyProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID, null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveByNonExistProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID, "NonExist");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveByProjectIdTaskException() throws SQLException {
        @Nullable final ProjectDTO project = new ProjectDTO();
        project.setUserId(USER_ID);
        projectService.getRepository().add(project);
        projectTaskService.removeProjectById(USER_ID, project.getId());
    }

    @Test
    public void testUnbindTaskToProject() throws SQLException {
        @Nullable String projectId = null;
        @Nullable String userId = null;
        @Nullable String taskId = null;
        for (@NotNull final TaskDTO task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            taskId = task.getId();
            break;
        }
        if (projectId == null && userId == null) return;
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        @Nullable TaskDTO task = taskService.findOneById(userId, taskId);
        Assert.assertNull(task.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskWithEmptyProjectId() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<TaskDTO> userTasks = taskService.findAll(USER_ID);
        if (userTasks == null) return;
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskWithEmptyTaskId() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<ProjectDTO> userProjects = projectService.findAll(USER_ID);
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskWithTaskNotFound() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<ProjectDTO> userProjects = projectService.findAll(USER_ID);
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID, projectId, "non-existent");
    }

}
