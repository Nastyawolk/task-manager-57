package ru.t1.volkova.tm.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.dto.model.ProjectDTO;
import ru.t1.volkova.tm.dto.model.UserDTO;
import ru.t1.volkova.tm.tm.migration.AbstractSchemeTest;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ProjectRepositoryTest extends AbstractSchemeTest  {

    private static int NUMBER_OF_ENTRIES = 5;

    private static String USER_ID_1;

    @Nullable
    private static List<ProjectDTO> projectList;

    @NotNull
    private static IProjectDTORepository getProjectDTORepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    @NotNull
    private static IUserDTORepository getUserDTORepository() {
        return context.getBean(IUserDTORepository.class);
    }

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final IUserDTORepository userRepository = getUserDTORepository();
        @NotNull final EntityManager entityManager = userRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull UserDTO user = new UserDTO();
            user.setFirstName("User");
            user.setLastName("User");
            user.setMiddleName("User");
            user.setEmail("user@mail.ru");
            user.setLogin("user");
            user.setPasswordHash("user");
            USER_ID_1 = user.getId();
            userRepository.add(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        createProjects();
    }

    private static void createProjects()  {
        projectList = new ArrayList<>();
        @NotNull final IProjectDTORepository projectRepository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectList = new ArrayList<>();
            for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
                @NotNull ProjectDTO project = new ProjectDTO();
                project.setName("Rproject" + i);
                project.setDescription("description" + i);
                project.setUserId(USER_ID_1);
                projectRepository.add(project);
                projectList.add(project);
            }
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testAddForUserId() {
        @NotNull final String name = "Test project";
        @NotNull final String description = "Test description";
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(USER_ID_1);
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.add(project);
            entityManager.getTransaction().commit();
            assertEquals(NUMBER_OF_ENTRIES+1, repository.getSize(USER_ID_1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAll() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll();
            assertEquals(projects.size(), repository.getSize());
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll(USER_ID_1);
            if (projects != null) {
                assertEquals(NUMBER_OF_ENTRIES, projects.size());
            }
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<ProjectDTO> projects = repository.findAll((String) null);
            if (projects != null) {
                assertEquals(0, projects.size());
            }
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIdForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (projectList == null) return;
            for (@NotNull final ProjectDTO project : projectList) {
                assertEquals(project, repository.findOneById(project.getUserId(), project.getId()));
            }
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (projectList == null) return;
            Assert.assertNull(repository.findOneById(null, projectList.get(1).getId()));
            Assert.assertNull(repository.findOneById(USER_ID_1, "NotExcitingId"));
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIndexForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (projectList == null) return;
            @Nullable final ProjectDTO project1 = repository.findOneByIndex(USER_ID_1, 1);
            @NotNull final ProjectDTO expected1 = projectList.get(1);
            assertEquals(expected1, project1);;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (projectList == null) return;
            @Nullable final ProjectDTO project1 = repository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES + 10);
            assertNull(project1);;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveOneByIdForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final ProjectDTO project = repository.findOneById(USER_ID_1, projectList.get(0).getId());
            repository.removeOneById(USER_ID_1, project.getId());
            entityManager.getTransaction().commit();
            assertNull(repository.findOneById(USER_ID_1, projectList.get(0).getId()));
            projectList.remove(0);
            NUMBER_OF_ENTRIES =  repository.findAll(USER_ID_1).size();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveAllForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(USER_ID_1);
            entityManager.getTransaction().commit();
            assertEquals(0, repository.getSize(USER_ID_1));
            createProjects();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveAllForUserNegative() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear("NotExcitingId");
            entityManager.getTransaction().commit();
            assertNotEquals(0, repository.getSize(USER_ID_1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testRemoveOneByIndexForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @Nullable final ProjectDTO project = repository.findOneByIndex(USER_ID_1, 1);
            repository.removeOneByIndex(USER_ID_1, 1);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_ID_1, project.getId()));
            projectList.remove(1);
            NUMBER_OF_ENTRIES =  repository.findAll(USER_ID_1).size();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final IProjectDTORepository repository = getProjectDTORepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            if (projectList == null) return;
            assertEquals(NUMBER_OF_ENTRIES , repository.getSize(USER_ID_1));
        } finally {
            entityManager.close();
        }
    }

}
