package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseUsername();

    @NotNull String getDatabaseDriver();

    @NotNull String getDatabaseDialect();

    @NotNull String getDatabaseHbm2ddlAuto();

    @NotNull String getDatabaseShowSql();

    @NotNull String getDatabaseUseSecondLvlCache();

    @NotNull String getDatabaseUseQueryCache();

    @NotNull String getDatabaseUseMinimalPuts();

    @NotNull String getDatabaseRegionPrefix();

    @NotNull String getDatabaseProviderCfgFileResPath();

    @NotNull String getDatabaseRegionFactoryClass();

}
