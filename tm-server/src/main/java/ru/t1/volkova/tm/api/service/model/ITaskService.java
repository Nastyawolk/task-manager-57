package ru.t1.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.model.ITaskRepository;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface ITaskService {

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    );

    @Nullable
    Task changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @NotNull Status status
    );

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @NotNull ITaskRepository getRepository();

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @Nullable
    List<Task> findAll(@Nullable String userId);

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator);

    void clear(@Nullable String userId);

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    void removeOneById(@Nullable String userId, @Nullable String id);

    void removeOneByIndex(@Nullable String userId, @Nullable Integer index);

}
