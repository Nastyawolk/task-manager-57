package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.api.repository.model.ITaskRepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractUserOwnedRepository<Task>
        implements ITaskRepository {

    @Override
    public @Nullable List<Task> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<Task> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator
    ) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId " +
                        "AND e.id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    public Task findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) {
            return;
        }
        removeOneById(userId, task.getId());
    }

    @Override
    public void clear(@Nullable final String userId) {
        @Nullable final List<Task> tasks = findAll(userId);
        if (tasks == null) return;
        for (@NotNull final Task task : tasks) {
            removeOneById(userId, task.getId());
        }
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @NotNull final String projectId
    ) {
        if (userId == null) return null;
        return entityManager
                .createQuery("SELECT e FROM Task e WHERE e.user.id = :userId AND e.projectId = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public @NotNull List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @Override
    public @Nullable List<Task> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Task e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Task.class)
                .getResultList();
    }

    @Override
    public void clear() {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks) {
            removeOneById(task.getId());
        }
    }

    @Override
    public @NotNull Task findOneById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public @Nullable Task findOneByIndex(@NotNull final Integer index) {
        return entityManager
                .createQuery("FROM Task", Task.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final Task task = findOneByIndex(index);
        if (task == null) {
            return;
        }
        removeOneById(task.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Task e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}
