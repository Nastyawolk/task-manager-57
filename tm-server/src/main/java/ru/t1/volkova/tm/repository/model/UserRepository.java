package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.api.repository.model.IUserRepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.model.User;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return entityManager
                .createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) {
        return entityManager
                .createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
                .setParameter("email", email)
                .getResultList().stream()
                .findFirst().orElse(null);
    }


    @Override
    public @NotNull List<User> findAll() {
        return entityManager.createQuery("FROM User", User.class).getResultList();
    }

    @Override
    public @Nullable List<User> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM User ORDER BY e." + param, User.class)
                .getResultList();
    }

    @Override
    public void clear() {
        @NotNull final List<User> users = findAll();
        for (@NotNull final User user : users) {
            removeOneById(user.getId());
        }
    }

    @Override
    @NotNull
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public @Nullable User findOneByIndex(
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("FROM User", User.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final User user = findOneByIndex(index);
        if (user == null) {
            return;
        }
        removeOneById(user.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM User e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}
