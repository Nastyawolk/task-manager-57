package ru.t1.volkova.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.volkova.tm.api.repository.model.IAbstractUserOwnedRepository;
import ru.t1.volkova.tm.api.service.model.IUserOwnedService;
import ru.t1.volkova.tm.exception.field.UserIdEmptyException;
import ru.t1.volkova.tm.model.AbstractUserOwnedModel;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IAbstractUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @Override
    public void add(@NotNull final M entity) {
        if (entity.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        super.add(entity);
    }

    @Override
    public void update(@NotNull final M entity) {
        if (entity.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        super.update(entity);
    }

}
