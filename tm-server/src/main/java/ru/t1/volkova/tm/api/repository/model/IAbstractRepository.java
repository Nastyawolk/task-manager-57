package ru.t1.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<E extends AbstractModel> {

    void add(@NotNull E entity);

    void update(@NotNull E entity);

    @NotNull
    List<E> findAll();

    @Nullable List<E> findAll(@NotNull Comparator comparator);

    void clear();

    @NotNull
    E findOneById(@NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull Integer index);

    void removeOneById(@NotNull String id);

    void removeOneByIndex(@NotNull Integer index);

    int getSize();

    @NotNull
    EntityManager getEntityManager();

}
