package ru.t1.volkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(@Nullable final String token) {
        super(token);
    }

}
