package ru.t1.volkova.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.volkova.tm.api.endpoint.*;

@Configuration
@ComponentScan("ru.t1.volkova.tm")
public class ClientConfiguration {

    @NotNull
    @Bean
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstanse();
    }

    @NotNull
    @Bean
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstanse();
    }

    @NotNull
    @Bean
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstanse();
    }

    @NotNull
    @Bean
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstanse();
    }

    @NotNull
    @Bean
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstanse();
    }

}
