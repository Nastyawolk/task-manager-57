package ru.t1.volkova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.volkova.tm.listener.AbstractListener;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.SystemUtil;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.volkova.tm.command";

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    @Getter
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    @Getter
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    @Getter
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    @Getter
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ApplicationContext context;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    public void start(@Nullable final String[] args) throws Exception {
        prepareStartup();
        processArguments(args);
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                @NotNull final String command = TerminalUtil.nextLine();
                System.out.println("[OK]");
                publisher.publishEvent(new ConsoleEvent(command));
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void processArguments(@Nullable final String[] args) throws Exception {
        if (args == null || args.length == 0) {
            return;
        }
        processArgument(args[0]);
        exit();
    }

    private void processArgument(@Nullable final String arg) throws Exception {
        if (arg == null) return;
        @Nullable AbstractListener abstractListener = null;
        for (@NotNull final AbstractListener listener : abstractListeners) {
            if (listener.getArgument() == null) continue;
            if (arg.equals(listener.getArgument())) {
                abstractListener = listener;
                System.out.println(abstractListener.getName());
                break;
            }
        }
        if (abstractListener == null) throw new ArgumentNotSupportedException(arg);
        abstractListener.handler(new ConsoleEvent(abstractListener.getName()));
    }

    public void exit() {
        System.exit(0);
    }

}
