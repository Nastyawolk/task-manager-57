package ru.t1.volkova.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @NotNull
    String getName();

    void setName(@Nullable String name);

}
