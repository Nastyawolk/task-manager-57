package ru.t1.volkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.volkova.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    private static final String DESCRIPTION = "Logout current user";

    @NotNull
    private static final String NAME = "logout";

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest();
        request.setToken(getToken());
        getAuthEndpoint().logout(request);
        setToken(null);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
